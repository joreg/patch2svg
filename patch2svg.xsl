<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
<xsl:variable name="scaling">15</xsl:variable> 
<xsl:variable name="fontsize">11</xsl:variable> 
<xsl:variable name="font">Arial</xsl:variable>
<xsl:variable name="pinsize">4</xsl:variable>
<xsl:variable name="pindistance">10</xsl:variable>
<xsl:variable name="halfpin">2</xsl:variable>
<xsl:variable name="nodeDefaultHeight">18</xsl:variable>
<xsl:variable name="ioboxDefaultHeight">15</xsl:variable>

<xsl:template match="/">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0, 0, 1024, 768" preserveAspectRatio="none" id="patchsvg">

<g id="viewport">
<!-- patch background -->
<rect fill="#E6E6E6" width="1024" height="768" />

<!-- draw links first so they are below nodes -->
<g>
<xsl:for-each select="/PATCH/LINK">
	<xsl:variable name="srcboundsType">
	<xsl:choose>
		<xsl:when test="/PATCH/NODE[@id=current()/@srcnodeid]/@componentmode='InABox'">
			<xsl:value-of select="'Box'" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="'Node'" />
		</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	<xsl:variable name="dstboundsType">
	<xsl:choose>
		<xsl:when test="/PATCH/NODE[@id=current()/@dstnodeid]/@componentmode='InABox'">
			<xsl:value-of select="'Box'" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="'Node'" />
		</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>

	<xsl:variable name="srcPinWidth">
	<xsl:choose>
		<xsl:when test="count(/PATCH/NODE[@id=current()/@srcnodeid]/PIN[@pintype='Input']) > count(/PATCH/NODE[@id=current()/@srcnodeid]/PIN[@pintype='Output'])">
			<xsl:value-of select="count(/PATCH/NODE[@id=current()/@srcnodeid]/PIN[@pintype='Input']) * $pindistance" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="count(/PATCH/NODE[@id=current()/@srcnodeid]/PIN[@pintype='Output']) * $pindistance" />
		</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="srcNameWidth">
	<xsl:choose>
		<xsl:when test="$srcPinWidth > /PATCH/NODE[@id=current()/@srcnodeid]/@textwidth">
			<xsl:value-of select="$srcPinWidth" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="/PATCH/NODE[@id=current()/@srcnodeid]/@textwidth" />
		</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>

	<xsl:variable name="srcNodeWidth">
	<xsl:choose>
		<xsl:when test="$srcNameWidth > /PATCH/NODE[@id=current()/@srcnodeid]/BOUNDS[@type=$srcboundsType]/@width div $scaling">
			<xsl:value-of select="$srcNameWidth" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="/PATCH/NODE[@id=current()/@srcnodeid]/BOUNDS[@type=$srcboundsType]/@width div $scaling" />
		</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>

	<xsl:variable name="dstPinWidth">
	<xsl:choose>
		<xsl:when test="count(/PATCH/NODE[@id=current()/@dstnodeid]/PIN[@pintype='Input']) > count(/PATCH/NODE[@id=current()/@dstnodeid]/PIN[@pintype='Output'])">
			<xsl:value-of select="count(/PATCH/NODE[@id=current()/@dstnodeid]/PIN[@pintype='Input']) * $pindistance" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="count(/PATCH/NODE[@id=current()/@dstnodeid]/PIN[@pintype='Output']) * $pindistance" />
		</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="dstNameWidth">
	<xsl:choose>
		<xsl:when test="$dstPinWidth > /PATCH/NODE[@id=current()/@dstnodeid]/@textwidth">
			<xsl:value-of select="$dstPinWidth" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="/PATCH/NODE[@id=current()/@dstnodeid]/@textwidth" />
		</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>	

	<xsl:variable name="dstNodeWidth">
	<xsl:choose>
		<xsl:when test="$dstNameWidth > /PATCH/NODE[@id=current()/@dstnodeid]/BOUNDS[@type=$dstboundsType]/@width div $scaling">
			<xsl:value-of select="$dstNameWidth" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="/PATCH/NODE[@id=current()/@dstnodeid]/BOUNDS[@type=$dstboundsType]/@width div $scaling" />
		</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="srcNodeTypeHeight">
	<xsl:choose>
		<xsl:when test="starts-with(/PATCH/NODE[@id=current()/@srcnodeid]/@nodename, 'IOBox')">
			<xsl:value-of select="$ioboxDefaultHeight" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$nodeDefaultHeight" />
		</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>

	<xsl:variable name="srcNodeHeight">
	<xsl:choose>
		<xsl:when test="$srcNodeTypeHeight > /PATCH/NODE[@id=current()/@srcnodeid]/BOUNDS[@type=$srcboundsType]/@height div $scaling">
			<xsl:value-of select="$srcNodeTypeHeight" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="/PATCH/NODE[@id=current()/@srcnodeid]/BOUNDS[@type=$srcboundsType]/@height div $scaling" />
		</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>

	<xsl:variable name="srcOutputCount">
	<xsl:choose>
		<xsl:when test="count(/PATCH/NODE[@id=current()/@srcnodeid]/PIN[@pintype='Output' and @visible=1]) > 1">
			<xsl:value-of select="count(/PATCH/NODE[@id=current()/@srcnodeid]/PIN[@pintype='Output' and @visible=1]) - 1" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="1" />
		</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>

	<xsl:variable name="dstInputCount">
	<xsl:choose>
		<xsl:when test="count(/PATCH/NODE[@id=current()/@dstnodeid]/PIN[@pintype='Input' and @visible=1]) > 1">
			<xsl:value-of select="count(/PATCH/NODE[@id=current()/@dstnodeid]/PIN[@pintype='Input' and @visible=1]) - 1" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="1" />
		</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>

	<xsl:variable name="srcPosX">
		<xsl:value-of select="/PATCH/NODE[@id=current()/@srcnodeid]/BOUNDS/@left div $scaling  + count(/PATCH/NODE[@id=current()/@srcnodeid]/PIN[@pinname=current()/@srcpinname]/preceding-sibling::PIN[@pintype='Output' and @visible=1]) * ($srcNodeWidth - $pinsize) div $srcOutputCount + $halfpin" />
	</xsl:variable>

	<xsl:variable name="srcPosY">
		<xsl:value-of select="/PATCH/NODE[@id=current()/@srcnodeid]/BOUNDS/@top div $scaling + $srcNodeHeight" />
	</xsl:variable>

	<xsl:variable name="dstPosX">
		<xsl:value-of select="/PATCH/NODE[@id=current()/@dstnodeid]/BOUNDS/@left div $scaling + count(/PATCH/NODE[@id=current()/@dstnodeid]/PIN[@pinname=current()/@dstpinname]/preceding-sibling::PIN[@pintype='Input' and @visible=1]) * ($dstNodeWidth - $pinsize) div $dstInputCount + $halfpin" />
	</xsl:variable>

	<xsl:variable name="dstPosY">
		<xsl:value-of select="/PATCH/NODE[@id=current()/@dstnodeid]/BOUNDS/@top div $scaling" />
	</xsl:variable>

	<rect x="{$srcPosX - $halfpin}" y="{$srcPosY}" width="{$pinsize}" height="1" />
	<xsl:choose>
		<xsl:when test="@linkstyle = 'Bezier'">
			<path fill="none" stroke="black" d=" ">
				<xsl:attribute name="d"><xsl:text>M</xsl:text><xsl:value-of select="$srcPosX" /><xsl:text> </xsl:text><xsl:value-of select="$srcPosY" /><xsl:text> Q </xsl:text><xsl:value-of select="LINKPOINT[1]/@x div $scaling" /><xsl:text> </xsl:text><xsl:value-of select="LINKPOINT[1]/@y div $scaling" /><xsl:text>, </xsl:text><xsl:value-of select="(LINKPOINT[2]/@x + LINKPOINT[1]/@x) div 2 div $scaling" /><xsl:text> </xsl:text><xsl:value-of select="(LINKPOINT[2]/@y + LINKPOINT[1]/@y) div 2 div $scaling" /><xsl:text> Q </xsl:text><xsl:value-of select="LINKPOINT[2]/@x div $scaling" /><xsl:text> </xsl:text><xsl:value-of select="LINKPOINT[2]/@y div $scaling" /><xsl:text>, </xsl:text><xsl:value-of select="$dstPosX" /><xsl:text> </xsl:text><xsl:value-of select="$dstPosY" /></xsl:attribute>
			</path>
			
		</xsl:when>
		<xsl:otherwise>
			<polyline fill="none" stroke="black" points="0 0">
			<xsl:attribute name="points"><xsl:value-of select="$srcPosX" /><xsl:text> </xsl:text><xsl:value-of select="$srcPosY" /><xsl:text> </xsl:text> <xsl:for-each select="LINKPOINT"><xsl:value-of select="@x div $scaling" /><xsl:text> </xsl:text><xsl:value-of select="@y div $scaling" /><xsl:text> </xsl:text></xsl:for-each><xsl:value-of select="$dstPosX" /><xsl:text> </xsl:text><xsl:value-of select="$dstPosY" /></xsl:attribute>
			</polyline>
		</xsl:otherwise>
	</xsl:choose>
	<rect fill="black" x="{$dstPosX - $halfpin}" y="{$dstPosY - 1}" width="{$pinsize}" height="1" />
 </xsl:for-each>
</g>

<!-- draw nodes -->
<g>
<xsl:for-each select="/PATCH/NODE">

<xsl:variable name="boundsType">
<xsl:choose>
	<xsl:when test="@componentmode='InABox'">
		<xsl:value-of select="'Box'" />
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="'Node'" />
	</xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:variable name="pinWidth">
<xsl:choose>
	<xsl:when test="count(PIN[@pintype='Input']) > count(PIN[@pintype='Output'])">
		<xsl:value-of select="count(PIN[@pintype='Input']) * $pindistance" />
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="count(PIN[@pintype='Output']) * $pindistance" />
	</xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:variable name="nameWidth">
<xsl:choose>
	<xsl:when test="$pinWidth > @textwidth">
		<xsl:value-of select="$pinWidth" />
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="@textwidth" />
	</xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:variable name="nodeWidth">
<xsl:choose>
	<xsl:when test="$nameWidth > BOUNDS[@type=$boundsType]/@width div $scaling">
		<xsl:value-of select="$nameWidth" />
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="BOUNDS[@type=$boundsType]/@width div $scaling" />
	</xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:variable name="nodeTypeHeight">
<xsl:choose>
	<xsl:when test="starts-with(@nodename, 'IOBox')">
		<xsl:value-of select="$ioboxDefaultHeight" />
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="$nodeDefaultHeight" />
	</xsl:otherwise>
</xsl:choose>
</xsl:variable>

<xsl:variable name="nodeHeight">
<xsl:choose>
	<xsl:when test="$nodeTypeHeight > BOUNDS[@type=$boundsType]/@height div $scaling">
		<xsl:value-of select="$nodeTypeHeight" />
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="BOUNDS[@type=$boundsType]/@height div $scaling" />
	</xsl:otherwise>
</xsl:choose>
</xsl:variable>

<g>
<!-- background -->
	<rect fill="#C0C0C0" width="{$nodeWidth}" height="{$nodeHeight}">
	<xsl:attribute name="x">
		<xsl:value-of select="BOUNDS[@type=$boundsType]/@left div $scaling" />
	</xsl:attribute>
	<xsl:attribute name="y">
		<xsl:value-of select="BOUNDS[@type=$boundsType]/@top div $scaling" />
	</xsl:attribute>
    </rect>

<!-- pinbars -->
    <xsl:if test="not(contains(@nodename, 'IOBox'))">
    <rect fill="#9A9A9A" height="{$pinsize}" width="{$nodeWidth}">
	<xsl:attribute name="x">
		<xsl:value-of select="BOUNDS/@left div $scaling" />
	</xsl:attribute>
	<xsl:attribute name="y">
		<xsl:value-of select="BOUNDS/@top div $scaling" />
	</xsl:attribute>
    </rect>
    <rect fill="#9A9A9A" height="{$pinsize}" width="{$nodeWidth}">
	<xsl:attribute name="x">
		<xsl:value-of select="BOUNDS/@left div $scaling" />
	</xsl:attribute>
	<xsl:attribute name="y">
		<xsl:value-of select="BOUNDS/@top div $scaling + $nodeHeight - $pinsize" />
	</xsl:attribute>
    </rect>
    </xsl:if>

<!-- nodename -->
    <text font-family="Arial" font-size="{$fontsize}">
	<xsl:attribute name="x">
		<xsl:value-of select="BOUNDS/@left div $scaling + $pinsize" />
	</xsl:attribute>
	<xsl:attribute name="y">
		<xsl:value-of select="BOUNDS/@top div $scaling + $nodeHeight - $pinsize - 1" />
	</xsl:attribute>
	<xsl:choose>
		<xsl:when test="@nodename='IOBox (String)'">
			<xsl:value-of select="PIN[@pinname='Input String']/@values" />
    		</xsl:when>
		<xsl:when test="@nodename='IOBox (Value Advanced)'">
			<xsl:value-of select="PIN[@pinname='Y Input Value']/@values" />
    		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="substring-before(@nodename, ' (')" />
    		</xsl:otherwise>
	</xsl:choose>
    </text>

<!-- draw pins -->
	<xsl:variable name="pinHeight">
	<xsl:choose>
		<xsl:when test="starts-with(@nodename, 'IOBox')">
			<xsl:value-of select="$pinsize div 2" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="$pinsize" />
		</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>

    <xsl:for-each select="PIN[(@pintype='Input' or @pintype='Output') and @visible=1]">
	<xsl:variable name="tipOffset">
	<xsl:choose>
		<xsl:when test="@pintype='Input'">
			<xsl:value-of select="-35" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="25" />
		</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="pinOffset">
	<xsl:choose>
		<xsl:when test="@pintype='Input'">
			<xsl:value-of select="$halfpin" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="- $pinsize - $halfpin" />
		</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="pinX">
	<xsl:choose>
		<xsl:when test="count(../PIN[@pintype='Input' and @visible=1]) = 1 and @pintype='Input'">
			<xsl:value-of select="../BOUNDS/@left div $scaling" />
		</xsl:when>
		<xsl:when test="count(../PIN[@pintype='Output' and @visible=1]) = 1 and @pintype='Output'">
			<xsl:value-of select="../BOUNDS/@left div $scaling" />
		</xsl:when>
		<xsl:when test="@pintype='Input'">
			<xsl:value-of select="../BOUNDS/@left div $scaling +  ($nodeWidth - $pinsize)  div (count(../PIN[@pintype='Input' and @visible=1]) - 1) * count(preceding-sibling::PIN[@pintype='Input' and @visible=1])" />
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="../BOUNDS/@left div $scaling + ($nodeWidth - $pinsize) div (count(../PIN[@pintype='Output' and @visible=1]) - 1) * count(preceding-sibling::PIN[@pintype='Output' and @visible=1])" />
		</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="pinY">
	<xsl:choose>
		<xsl:when test="@pintype='Output'">
			<xsl:value-of select="../BOUNDS/@top div $scaling + $nodeHeight - $pinHeight" />
			</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="../BOUNDS/@top div $scaling" />
			</xsl:otherwise>
	</xsl:choose>
	</xsl:variable>
	
	<!-- visible pin -->
	<rect fill="#666666" x="{$pinX}" y="{$pinY}" width="{$pinsize}" height="{$pinHeight}" />
	
	<!-- hover pin -->
	<rect opacity="0" fill="#666666" x="{$pinX - $pinsize div 2}" y="{$pinY - $pinsize div 2}" width="{$pinsize * 2}" height="{$pinsize * 2}" onmouseover="	document.getElementById('tiptext').textContent=evt.target.getAttribute('tooltip'); 
	var x = parseInt(evt.target.getAttribute('x')) + {$halfpin};
	document.getElementById('infotip').setAttribute('x', x);
	var y = parseInt(evt.target.getAttribute('y')) - {$pinOffset};
	document.getElementById('infotip').setAttribute('y', y);
	x -= document.getElementById('tiptext').getBBox().width * 0.5 + {$halfpin};
	y += {$tipOffset};
	document.getElementById('tooltip').setAttribute('transform', 'translate('+x+','+y+')'); document.getElementById('tipbg').setAttribute('width', document.getElementById('tiptext').getBBox().width);
	document.getElementById('tooltip').setAttribute('visibility', 'visible');
	document.getElementById('infotip').setAttribute('visibility', 'visible');" onmouseout="document.getElementById('tooltip').setAttribute('visibility', 'hidden');
	document.getElementById('infotip').setAttribute('visibility', 'hidden');">
	<xsl:attribute name="tooltip">
		<xsl:value-of select="concat(@pinname, ': ', @values)" />
	</xsl:attribute>
	</rect>
    </xsl:for-each>
</g>
 </xsl:for-each>
</g>

<!-- the tooltip -->
<g id="tooltip">
<rect id="tipbg" height="15" fill="#E6E6E6"/>
<text id="tiptext" y="12" font-family="Arial" font-size="{$fontsize}" />
</g>

<rect id="infotip" width="{$pinsize}" height="{$pinsize}" visibility="hidden" />

</g>
</svg>

</xsl:template>
</xsl:stylesheet>
